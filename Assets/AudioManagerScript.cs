﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerScript : MonoBehaviour
{
    [SerializeField]
    private AudioClip dontLeaveMeCharged;

    [SerializeField]
    private AudioClip dontLeaveMeUnCharged;

    [SerializeField]
    private AudioClip keepUpLads;

    [SerializeField]
    private AudioClip iAmWithYou;

    [SerializeField]
    private AudioClip aahHome;

    [SerializeField]
    private AudioClip boiledEgg;

    [SerializeField]
    private AudioClip charge;

    [SerializeField]
    private AudioClip discharge;

    [SerializeField]
    private AudioClip leave;

    [SerializeField]
    private AudioClip pick;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //play(boiledEgg);
    }

    private void play(AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.Play();
    }

    public void playLeftBehind(bool charged)
    {
        float rand = Random.Range(0.0f, 1.0f);

        print(rand);

        if (rand >= 0 && rand < 0.6)
            return;
        
        if (rand >= 0.6 && rand < 0.8)
            play(keepUpLads);
        else if(rand >= 0.8 && rand < 1.0)
        {
            if (charged)
            {
                if (rand >= 0.8 && rand < 0.9)
                    play(dontLeaveMeCharged);
                else
                    play(iAmWithYou);
            }
            else
                play(dontLeaveMeUnCharged);
        }
        
    }

    public void playAahHome()
    {
        float rand = Random.Range(0.0f, 1.0f);

        if (rand >= 0.5)
            play(aahHome);
    }

    public void playDischarge()
    {
        play(discharge);
    }

    public void playCharge()
    {
        play(charge);
    }

    public void playLeave()
    {
        play(leave);
    }

    public void playPick()
    {
        play(pick);
    }


}
