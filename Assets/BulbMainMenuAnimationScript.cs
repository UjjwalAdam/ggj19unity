﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulbMainMenuAnimationScript : MonoBehaviour
{
    bool blinking;

    [SerializeField]
    private Material lit;

    [SerializeField]
    private Material unlit;

    [SerializeField]
    private float blinkIn;

    [SerializeField]
    private float blinkFor;

    private MeshRenderer meshRenderer;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        StartCoroutine(blink());
    }

    IEnumerator blink()
    {
        meshRenderer.material = lit;        
        yield return new WaitForSeconds(blinkIn);
        meshRenderer.material = unlit;
        yield return new WaitForSeconds(blinkFor);
        meshRenderer.material = lit;
        yield return new WaitForSeconds(blinkFor);
        meshRenderer.material = unlit;
        yield return new WaitForSeconds(blinkFor);
        StartCoroutine(blink());
    }
}
