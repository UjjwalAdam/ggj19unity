﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FollowScript))]
public class ElectronFollowRange : Editor
{

    public void OnSceneGUI()
    {
        FollowScript followScript = ((FollowScript)target).GetComponent<FollowScript>();

        Handles.color = Color.red;
        Handles.DrawWireArc(followScript.transform.position, Vector3.up, Vector3.right, 360, followScript.followRange);
    }

}
