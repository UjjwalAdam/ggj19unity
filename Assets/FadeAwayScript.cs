﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeAwayScript : MonoBehaviour
{

    [SerializeField]
    private float fadeAwayIn;

    [SerializeField]
    private float fadeAwayTime;

    private Image myImage;

    private float currentTime;
    private bool startFade;

    private Text[] texts;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(disableMe());
        StartCoroutine(fadeDelay());
        myImage = GetComponent<Image>();

        currentTime = 0;

        texts = GetComponentsInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (startFade)
        {
            //print(1 - currentTime / fadeAwayTime);
            //float a = Mathf.Lerp(1, 0, );
            float frac = 1 - currentTime / fadeAwayTime;

            myImage.color = new Color(myImage.color.r, myImage.color.g, myImage.color.b, frac);

            foreach(Text t in texts)
            {
                t.color = new Color(t.color.r, t.color.g, t.color.b, frac);
            }

            currentTime += Time.deltaTime;
        }
    }


    IEnumerator disableMe()
    {
        yield return new WaitForSeconds(fadeAwayIn + fadeAwayTime);
        gameObject.SetActive(false);
    }

    IEnumerator fadeDelay()
    {
        yield return new WaitForSeconds(fadeAwayIn);
        startFade = true;
    }
}
