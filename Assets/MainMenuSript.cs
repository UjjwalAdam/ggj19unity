﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenuSript : MonoBehaviour
{

    private GameObject helpPanel;
    private GameObject storyPanel;

    [SerializeField]
    [TextArea]
    string[] description;

    [SerializeField]
    string[] titles;

    int storySlide;

    private Text storyTitle;
    private Text storyDescription;

    //private GameObject continueButton;

    private void Awake()
    {
        helpPanel = transform.Find("HelpPanel").gameObject;
        storyPanel = transform.Find("StoryPanel").gameObject;

        storyTitle = storyPanel.transform.Find("title").GetComponent<Text>();
        storyDescription = storyPanel.transform.Find("description").GetComponent<Text>();

        storySlide = 0;

        //continueButton = transform.Find("HelpPanel").Find("back").gameObject;
    }

    public void help()
    {
        helpPanel.SetActive(true);
        //EventSystem.current.SetSelectedGameObject(continueButton);
    }

    public void play()
    {
        storyPanel.SetActive(true);

        storyTitle.text = titles[storySlide];
        storyDescription.text = description[storySlide];
        
    }

    public void helpBack()
    {
        helpPanel.SetActive(false);
    }

    public void quit()
    {
        Application.Quit();
    }

    public void continueStory()
    {
        storySlide++;

        if (storySlide >= titles.Length)
        {
            SceneManager.LoadScene("Tutorial");
            return;
        }

        storyTitle.text = titles[storySlide];
        storyDescription.text = description[storySlide];
    }

}
