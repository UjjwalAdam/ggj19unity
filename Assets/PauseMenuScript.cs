﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{

    [SerializeField]
    private bool pause;

    private GameObject pauseMenu;
    private Text timeLeftText;
    private GameObject HUD;

    private GameObject gameOverPanel;
    private Text survivedForText;

    private GameMaster gameMaster;

    // Start is called before the first frame update
    void Awake()
    {
        pauseMenu = transform.Find("PauseMenuPanel").gameObject;
        HUD = transform.Find("HUD").gameObject;
        timeLeftText = HUD.transform.Find("timeleft").gameObject.GetComponent<Text>();
        gameOverPanel = transform.Find("GameOverPanel").gameObject;
        survivedForText = gameOverPanel.transform.Find("survivedfor").gameObject.GetComponent<Text>();

        gameMaster = GameObject.FindGameObjectWithTag("gamemaster").GetComponent<GameMaster>();
    }


    bool gameOverCalled = false;
    // Update is called once per frame
    void Update()
    {
        //pause menu
        if (Input.GetButtonUp("Cancel") && Time.timeScale > 0)
        {
            pause = !pause;
            pauseMenu.SetActive(pause);

            if (pause)
                Time.timeScale = 0.0f;
            else
                Time.timeScale = 1.0f;
        }

        //hud
        if (gameMaster.startWarning)
        {
            timeLeftText.text = (int)gameMaster.currentWarningCountDown + "s";

            if(!HUD.activeSelf && gameMaster.warningTime - gameMaster.currentWarningCountDown >= 2.0f)
            {
                HUD.SetActive(true);
            }
        }
        else
        {
            HUD.SetActive(false);
        }

        //gameover
        if (gameMaster.gameOver && !gameOverCalled)
        {
            gameOverPanel.SetActive(true);
            Time.timeScale = 0;
            survivedForText.text = string.Format("You Survived for {0}s", (int)gameMaster.survivingTime);
            gameOverCalled = true;
        }

    }

    public void Resume()
    {
        pause = false;
        Time.timeScale = 1.0f;
        pauseMenu.SetActive(false);
    }

    public void Restart()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
