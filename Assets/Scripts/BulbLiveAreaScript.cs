﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulbLiveAreaScript : MonoBehaviour
{

    public bool needElectrons;

    private void OnTriggerStay(Collider other)
    {
        if (needElectrons 
            && other.gameObject.tag == "electron" 
            && other.gameObject.GetComponent<ElectronScript>().isCharged() 
            && !other.gameObject.GetComponent<FollowScript>().following)
        {
            needElectrons = false;
            transform.parent.GetComponent<BulbScript>().addElectrons();
            Destroy(other.gameObject);
        }
    }

}
