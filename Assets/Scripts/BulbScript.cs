﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulbScript : MonoBehaviour
{
    [SerializeField]
    private int electronsStored;

    [SerializeField]
    private int maxElectronsStore;

    private BulbLiveAreaScript liveAreaScript;

    [SerializeField]
    private float consumeTimeSecs;

    public bool isWorking;

    [SerializeField]
    private GameObject electronPrefab;

    private Transform groundSpawn;
    private Transform electronGroup;

    [SerializeField]
    private Material litBulbMat;

    [SerializeField]
    private Material unlitBulbMat;

    private MeshRenderer bulbLightMesh;

    private AudioManagerScript audioManager;

    private void Awake()
    {
        liveAreaScript = transform.Find("LiveArea").GetComponent<BulbLiveAreaScript>();
        groundSpawn = transform.Find("GroundSpawn").transform;
        electronGroup = GameObject.FindGameObjectWithTag("electrongroup").transform;

        bulbLightMesh = transform.Find("Bulb 1").Find("Bulb").gameObject.GetComponent<MeshRenderer>();

        bulbLightMesh.material = isWorking ? litBulbMat : unlitBulbMat;

        audioManager = GameObject.FindGameObjectWithTag("gamemaster").GetComponent<AudioManagerScript>();
    }

    public void addElectrons()
    {
        electronsStored++;
    }

    private void FixedUpdate()
    {
        if(electronsStored < maxElectronsStore && !liveAreaScript.needElectrons)
        {
            liveAreaScript.needElectrons = true;
        }

        if (!isWorking && electronsStored > 0)
        {
            StartCoroutine(startWorking());
        }
    }

    IEnumerator startWorking()
    {
        isWorking = true;

        bulbLightMesh.material = litBulbMat;

        yield return new WaitForSeconds(consumeTimeSecs);
        electronsStored--;
        isWorking = false;
        bulbLightMesh.material = unlitBulbMat;
        GameObject newElectron = Instantiate(electronPrefab, groundSpawn.position, groundSpawn.rotation, electronGroup);
        newElectron.GetComponent<ElectronScript>().setCharge(false);
        audioManager.playDischarge();
    }


}
