﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronPickupScript : MonoBehaviour {

    private FollowAreaScript followArea;

    private void Awake()
    {
        followArea = transform.Find("FollowAreaHB").GetComponent<FollowAreaScript>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("O"))
        {
            followArea.setPickCharged();
        }

        if (Input.GetButtonDown("P"))
        {
            followArea.setPickUncharged();
        }

        if (Input.GetButtonDown("K"))
        {
            followArea.setLeaveCharged();
        }

        if (Input.GetButtonDown("L"))
        {
            followArea.setLeaveUncharged();
        }
    }


}
