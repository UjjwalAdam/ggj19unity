﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronScript : MonoBehaviour {

    [SerializeField]
    private bool charged;

    [SerializeField]
    private Material liveMaterial;

    [SerializeField]
    private Material unchargedMaterial;

    private MeshRenderer meshRenderer;

    private void Awake()
    {
        meshRenderer = transform.Find("Electron").GetComponent<MeshRenderer>() ;

        meshRenderer.material = charged ? liveMaterial : unchargedMaterial;
    }


    public void setCharge(bool charged)
    {
        this.charged = charged;
        meshRenderer.material = charged ? liveMaterial : unchargedMaterial;
        
    }

    public bool isCharged()
    {
        return charged;
    }

}
