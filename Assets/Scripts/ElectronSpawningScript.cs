﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronSpawningScript : MonoBehaviour {

    [SerializeField]
    private bool spawn;

    [SerializeField]
    private bool charging;

    [SerializeField]
    private GameObject electronPrefab;
    
    private Transform spawnPoint;
    private GameObject parent;
    private PickupAreaHitboxScript pickupArea;

    [SerializeField]
    private float spawnInTime;

    private AudioManagerScript audioManager;

    // Use this for initialization
    void Awake () {
        spawnPoint = transform.Find("ElectronSpawnPoint");
        parent = GameObject.FindGameObjectWithTag("electrongroup");
        pickupArea = transform.Find("PickupArea").gameObject.GetComponent<PickupAreaHitboxScript>();

        audioManager = GameObject.FindGameObjectWithTag("gamemaster").GetComponent<AudioManagerScript>();
    }
	
	// Update is called once per frame
	void Update () {

        if (!charging && pickupArea.findAndDeleteOne())
        {
            spawn = true;
        }

        if (spawn)
        {
            spawn = false;
            StartCoroutine(SpawnInTime());
        }

	}

    IEnumerator SpawnInTime()
    {
        charging = true;
        yield return new WaitForSeconds(spawnInTime);
        Instantiate(electronPrefab, spawnPoint.position, spawnPoint.rotation, parent.transform);
        charging = false;
        audioManager.playCharge();
    }

    


}
