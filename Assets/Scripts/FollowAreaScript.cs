﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowAreaScript : MonoBehaviour {


    private bool pickCharged;
    private bool pickUncharged;
    
    private bool leaveCharged;
    private bool leaveUncharged;
    
    private float resetTime = 2.0f;

    private AudioManagerScript audioManager;

    private void Awake()
    {
        audioManager = GameObject.FindGameObjectWithTag("gamemaster").GetComponent<AudioManagerScript>();
    }

    public void setPickCharged()
    {
        StartCoroutine(startPickCharged());
    }

    public void setPickUncharged()
    {
        StartCoroutine(startPickUncharged());
    }

    public void setLeaveCharged()
    {
        StartCoroutine(startLeaveCharged());
    }

    public void setLeaveUncharged()
    {
        StartCoroutine(startLeaveUncharged());
    }


    private IEnumerator startPickCharged()
    {
        pickCharged = true;
        yield return new WaitForSeconds(resetTime);
        pickCharged = false;
    }

    private IEnumerator startPickUncharged()
    {
        pickUncharged = true;
        yield return new WaitForSeconds(resetTime);
        pickUncharged = false;
    }

    private IEnumerator startLeaveCharged()
    {
        leaveCharged = true;
        yield return new WaitForSeconds(resetTime);
        leaveCharged = false;
    }

    private IEnumerator startLeaveUncharged()
    {
        leaveUncharged = true;
        yield return new WaitForSeconds(resetTime);
        leaveUncharged = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "electron")
        {
            bool charged = other.gameObject.GetComponent<ElectronScript>().isCharged();
            FollowScript followScript = other.gameObject.GetComponent<FollowScript>();


            if (pickCharged && charged && !followScript.following)
            {
                followScript.following = true;
                pickCharged = false;

                audioManager.playPick();
            }
            else if(pickUncharged && !charged && !followScript.following)
            {
                followScript.following = true;
                pickUncharged = false;

                audioManager.playPick();
            }
            else if(leaveCharged && charged && followScript.following)
            {
                followScript.following = false;
                leaveCharged = false;

                audioManager.playLeave();

            }
            else if(leaveUncharged && !charged && followScript.following)
            {
                followScript.following = false;
                leaveUncharged = false;

                audioManager.playLeave();
            }
        }
    }

}
