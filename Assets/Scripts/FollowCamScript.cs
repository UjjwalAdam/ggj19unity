﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamScript : MonoBehaviour {


    private Transform target;

    private Vector3 delta;

	// Use this for initialization
	void Awake () {
        target = GameObject.FindGameObjectWithTag("hero").transform;
        delta = this.transform.position - target.position;
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 myPos = this.transform.position;
        
        this.transform.position = new Vector3(delta.x + target.position.x, delta.y, delta.z + target.position.z);

	}
}
