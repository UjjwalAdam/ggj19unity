﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowScript : MonoBehaviour {

    private Transform target;

    public bool following;

    [SerializeField]
    private float speed;

    public float followRange;

    private AudioManagerScript audioManager;

	// Use this for initialization
	void Awake () {
        target = GameObject.FindGameObjectWithTag("hero").transform;
        audioManager = GameObject.FindGameObjectWithTag("gamemaster").GetComponent<AudioManagerScript>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        transform.LookAt(target);   


        if (following)
            transform.Translate(Vector3.forward * speed * Time.deltaTime);


        if (following && Vector3.Distance(target.position, transform.position) > followRange)
        {
            following = false;
            audioManager.playLeftBehind(GetComponent<ElectronScript>().isCharged());
        }
		
	}
}
