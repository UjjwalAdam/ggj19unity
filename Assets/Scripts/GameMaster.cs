﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{

    private List<BulbScript> bulbs;

    [HideInInspector]
    public bool startWarning;

    //for leaderboards
    public float survivingTime;

    public float warningTime;

    [HideInInspector]
    public float currentWarningCountDown;

    [SerializeField]
    private float checkingRate;

    public bool gameOver;

    private void Awake()
    {
        bulbs = new List<BulbScript>();

        foreach(GameObject bs in GameObject.FindGameObjectsWithTag("bulb"))
        {
            bulbs.Add(bs.GetComponent<BulbScript>());
        }

        currentWarningCountDown = warningTime;


    }


    private bool checking;

    private void Update()
    {
        if (!checking)
            StartCoroutine(check());

        if (startWarning)
        {

            if (currentWarningCountDown < 0)
            {
                gameOver = true;
                print("Game Over");
            }
            else
            {
                print("Warning started, light bulbs fast");
                currentWarningCountDown -= Time.deltaTime;
            }
        }
        else
        {
            currentWarningCountDown = warningTime;
        }

        if (!gameOver)
            survivingTime += Time.deltaTime;
    }

    
    IEnumerator check()
    {
        checking = true;

        bool temp = false;
        foreach(BulbScript b in bulbs)
        {
            if (!b.isWorking)
                temp = true;
        }
        startWarning = temp;


        yield return new WaitForSeconds(1 / checkingRate);
        checking = false;
    }



}
