﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroMovementScript : MonoBehaviour {

    [SerializeField]
    private float maxSpeedMag;

    [SerializeField]
    private float deltaSpeed;

    [SerializeField]
    private Vector3 currentSpeed;

    private float xInput;
    private float yInput;

    Rigidbody rb;

    // Use this for initialization
    void Awake () {
        rb = GetComponent<Rigidbody>();
	}

    void Update()
    {
        xInput = Input.GetAxis("Horizontal");
        yInput = Input.GetAxis("Vertical");
    }

    // Update is called once per frame
    void FixedUpdate () {
        
        currentSpeed += new Vector3(xInput*deltaSpeed*Time.deltaTime, 0, yInput*deltaSpeed*Time.deltaTime);
        currentSpeed = Vector3.ClampMagnitude(currentSpeed, maxSpeedMag);

        if (xInput == 0.0 || Mathf.Sign(xInput) * Mathf.Sign(currentSpeed.x) == -1)
            currentSpeed = Vector3.Scale(currentSpeed, new Vector3(0.8f, 1, 1));

        if (yInput == 0.0 || Mathf.Sign(yInput) * Mathf.Sign(currentSpeed.z) == -1)
            currentSpeed = Vector3.Scale(currentSpeed, new Vector3(1, 1, 0.8f));

        //transform.Translate(currentSpeed);
        rb.velocity = new Vector3(currentSpeed.x, rb.velocity.y, currentSpeed.z);
    }
}
