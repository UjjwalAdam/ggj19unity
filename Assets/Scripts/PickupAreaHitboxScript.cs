﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupAreaHitboxScript : MonoBehaviour {

    private Stack<GameObject> electronsInArea;

    private AudioManagerScript audioManager;

    private void Awake()
    {
        electronsInArea = new Stack<GameObject>();
        audioManager = GameObject.FindGameObjectWithTag("gamemaster").GetComponent<AudioManagerScript>();
    }

    public bool findAndDeleteOne()
    {

        if (electronsInArea.Count <= 0)
            return false;

        Destroy(electronsInArea.Pop());
        return true;
        

    }

    private void OnTriggerStay(Collider col)
    {
        if(col.gameObject.tag == "electron")
        {
            if (!electronsInArea.Contains(col.gameObject) 
                && !col.gameObject.GetComponent<FollowScript>().following 
                && !col.gameObject.GetComponent<ElectronScript>().isCharged())
            {
                electronsInArea.Push(col.gameObject);
            }
        }
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "hero")
        {
            audioManager.playAahHome();
        }
    }

}
