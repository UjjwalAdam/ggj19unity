﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialEventSystem : MonoBehaviour
{

    [SerializeField]
    [TextArea]
    private string[] tutorialTextStrings;

    private int count;


    [SerializeField]
    private float timeToCompleteTotorial;

    private GameObject tutorialPanel;
    private GameObject tutorialText;
    private GameObject tutorialCompletePanel;

    private void Awake()
    {
        tutorialPanel = transform.Find("TutorialPanel").gameObject;
        tutorialText = tutorialPanel.transform.Find("TutorialText").gameObject;
        tutorialCompletePanel = transform.Find("TutorialCompletePannel").gameObject;
        count = 0;
        tutorialText.GetComponent<Text>().text = tutorialTextStrings[count];

        StartCoroutine(autoNextSlide());
    }

    private bool startedFinishCount;
    private void Update()
    {
        if (Input.GetButtonUp("Next"))
        {
            nextSlide();
        }
    }

    [SerializeField]
    float autoNextTileTime = 20.0f;

    IEnumerator autoNextSlide()
    {
        int temp = count;
        yield return new WaitForSeconds(autoNextTileTime);
        if (temp == count)
            nextSlide();

        if (count < tutorialTextStrings.Length)
            StartCoroutine(autoNextSlide());
    }

    private void nextSlide()
    {
        if (++count >= tutorialTextStrings.Length)
        {
            tutorialPanel.SetActive(false);
            if (!startedFinishCount)
                StartCoroutine(startFinishCount());
        }
        else
            tutorialText.GetComponent<Text>().text = tutorialTextStrings[count];
    }

    IEnumerator startFinishCount()
    {
        startedFinishCount = true;
        yield return new WaitForSeconds(timeToCompleteTotorial);
        //PlayerPrefs.SetInt("played_tutorial", 1);
        tutorialCompletePanel.SetActive(true);
        Time.timeScale = 0.0f;
    }

    public void nextLvl()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Lvl1");
    }

}
